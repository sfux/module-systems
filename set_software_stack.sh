#!/bin/bash

#####################################################################
# This script allows the users to change the default software stack #
# on the Euler cluster when logging in                              #
# Author: Samuel Fux                                                #
# Developed @ETH Zurich in September 2019                           #
#####################################################################

# Uncomment for debugging
#set -x

#############################################################################################################
# Overview on variables  :                                                                                  #
#                                                                                                           #
# USER_FILE              : File containing the users default for initializing the software stack on Euler   #
# SILENT                 : Parameter to determine if output is printed to stdout or if the script is silent #
# GLOB_DEFAULT           : Global default for initializing the software stack on Euler                      #
# USER_DEFAULT           : User default for initializing the software stack on Euler                        #
# USER_CHOICE            : New default for software stack to be set
#############################################################################################################

# Initialize variables
USER_FILE=$HOME/.software_stack_default
SILENT=0
GLOB_DEFAULT="none"
USER_DEFAULT="none"
USER_CHOICE="none"

# Function to display help message for this script
display_help()
{
cat <<-EOF
$0: Command to set the old (environment modules) or new (LMOD modules) software stack as default upon login on the Euler cluster

Usage: set_software_stack.sh [-h] [-s] [-i] [old | new]

Options:

        -h | --help                      Display this help message and exit
        -s | --silent                    Do not print anything to stdout except errors when changing the default for the software stack
        -i | --info                      Show which software stack is set as default and exit
        old | new                        Choices for the default software stack

Examples:

        Set the new software stack as default:

                set_software_stack.sh new

        Set the old software stack as default:

                set_software_stack.sh old

        Show which software stack is set as default

                set_software_stack.sh -i

Documentation of module commands:

        We provide a general overview on module commands on our wiki.

                https://scicomp.ethz.ch/wiki/Setting_up_your_environment

Old software stack (environment modules):

        The old software stack (2014-2021+) on Euler uses environment modules. An overview on available
        modules can be found on our wiki:

                https://scicomp.ethz.ch/wiki/Category:Application
                https://scicomp.ethz.ch/wiki/Euler_applications

New software stack (LMOD modules):

        The new software stack (2018-2021+) on Euler uses LMOD modules. LMOD modules provide additional
        features like a hierarchy of modules that prevents users from mixing different toolchains:

                https://scicomp.ethz.ch/wiki/New_SPACK_software_stack_on_Euler

        An overview on the available modules can be found on our wiki:

                https://scicomp.ethz.ch/wiki/Euler_applications_and_libraries

        List of all software not currently available in the new software stack

                https://scicomp.ethz.ch/wiki/Difference_between_old_and_new_software_stacks_on_Euler

See also:

        env2lmod; switch from old to new software stack in the shell (change not permanent)
        lmod2env; switch from the new to the old software stack in the shell (change not permanent)

EOF
exit 1
}

# Checking the global default. If not found, inform the user and set the default to "none"
if [ -f "/etc/profile.d/software_stack_default.sh" ]; then
        GLOB_DEFAULT=`grep -m 1 SOFTWARE_STACK_DEFAULT= /etc/profile.d/software_stack_default.sh  | cut -d "=" -f 2`
else
        echo -e "Warning: File /etc/profile.d/software_stack_default.sh, that should contain the global default for the software stack does not exist on this node. Cannot determine the global default \n"
fi

# Checking for user default
if [ -f "$USER_FILE" ]; then
        # User has a .software_stack_default file in his home directory take value from there
        USER_DEFAULT=$(head -n 1 $USER_FILE)
        case $USER_DEFAULT in
                old)
                ;;
                new)
                ;;
                *)
                echo -e "Warning: $USER_DEFAULT from $HOME/.software_stack_default is not a valid choice. Setting user default to 'none' \n"
                USER_DEFAULT="none"
                ;;
        esac
else
        USER_DEFAULT="none"
fi

# Display info about (global or user) default for initializing the software stack on Euler
display_info()
{
case $USER_DEFAULT in
        none)
                echo -e "You have not set any default for the software stack, therefore the global default applies"
                echo -e "The global default is set to: $GLOB_DEFAULT"
        ;;
        old|new)
                echo -e "The global default is set to: $GLOB_DEFAULT"
                echo -e "You have set the $USER_DEFAULT software stack as your personal default, which supersedes the global default"
                
        ;;
        *)
        ;;
esac
exit 1
}

# Parse command line arguments, but first check if there are any
if [[ $# -eq 0 ]]; then
        display_help
fi

while [[ $# -gt 0 ]]
do
        case $1 in
                -h|--help)
                shift
                display_help
                ;;
                -s|--silent)
                SILENT=1
                shift
                ;;
                -i|--info)
                shift
                display_info
                ;;
                new)
                USER_CHOICE=new
                shift
                ;;
                old)
                USER_CHOICE=old
                shift
                ;;
                *)
                echo -e "Warning: ignoring unknown option $1 \n"
                shift
                ;;
        esac
done

# Check if default for software stack was specified
case $USER_CHOICE in
        none)
        echo -e "Warning: no value for the default software stack has been set"
        echo -e "Please run the command again and specify a software stack [old|new] to be set as default"
        display_help
        ;;
        old)
                if [ "$SILENT" == "0" ]
                then
                        echo -e "You are setting the old software stack as default upon login on Euler"
                fi
                echo "old" > $USER_FILE
        ;;
        new)
                if [ "$SILENT" == "0" ]
                then
                        echo -e "You are setting the new software stack as default upon login on Euler"
                fi
                echo "new" > $USER_FILE
        ;;
        *)
        ;;
esac
