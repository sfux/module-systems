#!/bin/bash

########################################################################################
#                                                                                      #
# Script to switch local enviornment from environment modules to LMOD modules on Euler #
# This implies also a switch of the standard compiler from GCC 4.8.2 to GCC 4.8.5      #
# Author: Dr. Sc. ETH Samuel Fux                                                       #
# Date  : August,2018@ETH Zurich                                                       #
#                                                                                      #
########################################################################################

# Check what module system is used in the current shell
case $MODULESHOME in
       /cluster/apps/modules)
       MS_CURRENT=env
       ;;
       /cluster/spack/apps/linux-centos7-x86_64/gcc-4.8.5/lmod*)
       MS_CURRENT=lmod
       ;;
       /cluster/apps/gcc-4.8.5/lmod*)
       MS_CURRENT=lmod
       ;;
       *)
       echo -e "Current modulesystem could not be determined"
       echo -e "\$MODULESHOME=$MODULESHOME"
       echo -e "Please logout and login again"
       return
       ;;
esac

# The code for the functions prepend_path, append_path and remove path is taken from the following stackoverflow discussion
# https://stackoverflow.com/questions/24515385/is-there-a-general-way-to-add-prepend-remove-paths-from-general-environment-vari

# generic function to prepend a path to an environment variable
prepend_path() {
    local varName=$1 fieldVal=$2 IFS=${3:-':'} auxArr
    read -ra auxArr <<< "${!varName}"
    for i in "${!auxArr[@]}"; do
        [[ ${auxArr[i]} == "$fieldVal" ]] && unset auxArr[i]
    done
    auxArr=("$fieldVal" "${auxArr[@]}")
    printf -v "$varName" '%s' "${auxArr[*]}"
}

# generic function to append a path to an environment variable
append_path() {
    local varName=$1 fieldVal=$2 IFS=${3:-':'} auxArr
    read -ra auxArr <<< "${!varName}"
    for i in "${!auxArr[@]}"; do
        [[ ${auxArr[i]} == "$fieldVal" ]] && unset auxArr[i]
    done
    auxArr+=("$fieldVal")
    printf -v "$varName" '%s' "${auxArr[*]}"
}

# generic function to remove a path from an environment variable
remove_path() {
    local varName=$1 fieldVal=$2 IFS=${3:-':'} auxArr
    read -ra auxArr <<< "${!varName}"
    for i in "${!auxArr[@]}"; do
        [[ ${auxArr[i]} == "$fieldVal" ]] && unset auxArr[i]
    done
    printf -v "$varName" '%s' "${auxArr[*]}"
}

if [ "$MS_CURRENT" == "lmod"  ]
then
        echo -e "Current modulesystem is already LMOD modules, nothing to change for env2lmod"
        return
fi

# step 1:
# clean up all traces of environment modules
module purge
unset -f module
unset MODULESHOME
unset LOADEDMODULES
unset MODULEPATH

# step 2:
# clean up all traces of GCC 4.8.2 being the standard compiler
remove_path INCLUDE /cluster/apps/gcc/4.8.2/include
remove_path PATH /cluster/apps/gcc/4.8.2/bin
remove_path LD_LIBRARY_PATH /cluster/apps/gcc/4.8.2/lib64

# step 3:
# unset functions to modify environment variables
unset -f remove_path
unset -f append_path
unset -f prepend_path

# step 4:
# no need to set GCC 4.8.5 as standard compiler as it is already the system compiler
# initialize LMOD
export LMOD_BASEPATH=/cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod
export LMOD_sys=`uname`
export MODULEPATH_ROOT="/cluster/apps/lmodules"
export MODULEPATH=$($LMOD_BASEPATH/libexec/addto --append MODULEPATH /cluster/apps/lmodules/Core)
export BASH_ENV=$LMOD_BASEPATH/init/bash
prepend_path MANPATH $LMOD_BASEPATH/share/man

PS_CMD=/bin/ps
EXPR_CMD=/bin/expr
BASENAME_CMD=/bin/basename

[[ ${-/x} != $- ]] && XTRACE_STATE="-x"  || XTRACE_STATE="+x"
[[ ${-/v} != $- ]] && VERBOSE_STATE="-v" || VERBOSE_STATE="+v"
set +xv     # Turn off tracing for now.

export LMOD_PKG=$LMOD_BASEPATH
export LMOD_DIR=$LMOD_BASEPATH/libexec
export LMOD_CMD=$LMOD_BASEPATH/libexec/lmod
export MODULESHOME=$LMOD_BASEPATH

export LMOD_SETTARG_FULL_SUPPORT=no

module()
{
  eval $($LMOD_CMD bash "$@") && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
}

export LMOD_VERSION="7.7.13"

if [ "${LMOD_SETTARG_CMD:-:}" != ":" ]; then
  settarg () {
    eval $(${LMOD_SETTARG_CMD:-:} -s sh "$@" )
  }
fi

unalias ml 2> /dev/null || true
ml()
{
  eval $($LMOD_DIR/ml_cmd "$@")
}

if [ -n "${BASH_VERSION:-}" -a "yes" != no ]; then
  export -f module
  export -f ml
fi
unset export_module

clearMT()
{
  eval $($LMOD_DIR/clearMT_cmd bash)
}

xSetTitleLmod()
{
  builtin echo -n -e "\033]2;$1\007";
}

export SHOST=${SHOST-${HOSTNAME%%.*}}

. /cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/init/lmod_bash_completions

set  $XTRACE_STATE $VERBOSE_STATE
unset XTRACE_STATE  VERBOSE_STATE

unset PS_CMD EXPR_CMD BASENAME_CMD

if [ -z "$__Init_Default_Modules" ]; then
   export __Init_Default_Modules=1;

   ## ability to predefine elsewhere the default list
   LMOD_SYSTEM_DEFAULT_MODULES="StdEnv:gcc/4.8.5"
   export LMOD_SYSTEM_DEFAULT_MODULES
   module --initial_load --no_redirect restore
else
   module refresh
fi
