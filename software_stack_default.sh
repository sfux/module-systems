#!/bin/bash

###############################################################
# This script contains the default setting if the old or the  #
# new software stack will be initialized in the users shell   #
# Author: Samuel Fux                                          #
# Developed @ETH Zurich in September 2019                     #
###############################################################

# set aliases for the scripts to switch between software stacks
alias env2lmod=". /cluster/apps/local/env2lmod.sh"
alias lmod2env=". /cluster/apps/local/lmod2env.sh"

# This variable determines the default.
# old -> environment modules
# new -> LMOD modules
SOFTWARE_STACK_DEFAULT=old

# location where the profile files for environment modules and LMOD modules are stored
PROFILE_DIR=/cluster/apps/.moduleprofiles

# PATH of the file that can overwrite the default setting with regards to the software stack
USER_FILE=$HOME/.software_stack_default

###############################################################
# Don't change the following part. All configurations should  #
# be done by modifying the variables defined up to here       #
###############################################################

# The code for the functions prepend_path is taken from the following stackoverflow discussion
# https://stackoverflow.com/questions/24515385/is-there-a-general-way-to-add-prepend-remove-paths-from-general-environment-vari

# generic function to prepend a path to an environment variable
prepend_path() {
    local varName=$1 fieldVal=$2 IFS=${3:-':'} auxArr
    read -ra auxArr <<< "${!varName}"
    for i in "${!auxArr[@]}"; do
        [[ ${auxArr[i]} == "$fieldVal" ]] && unset auxArr[i]
    done
    auxArr=("$fieldVal" "${auxArr[@]}")
    printf -v "$varName" '%s' "${auxArr[*]}"
}

# Module system that will be initialized in the shell. Initialize the variable with the default value
SOFTWARE_STACK=$SOFTWARE_STACK_DEFAULT

# check first if the user has a file in his home directory that overrides the default
# if the file does not exist, go with the default, if it exists take the value from the file
# if the value from the file is valid, use it, otherwise fall back to the default
if [ -f "$USER_FILE" ]; then
        # User has a .module_default file in his home directory take value from there
        SOFTWARE_STACK=$(head -n 1 $USER_FILE)
        case $SOFTWARE_STACK in
                old)
                        SOFTWARE_STACK=old
                ;;
                new)
                        SOFTWARE_STACK=new
                ;;
                *)
                        echo "$SOFTWARE_STACK from $HOME/.software_stack_default is not a valid software stack"
                        echo "Using global default: $SOFTWARE_STACK_DEFAULT"
                        SOFTWARE_STACK=$SOFTWARE_STACK_DEFAULT
                ;;
        esac
fi

# Determine which profile file needs to be sourced
case $SOFTWARE_STACK in
        old)
                # again set gcc/4.8.2 as standard compiler
                prepend_path INCLUDE /cluster/apps/gcc/4.8.2/include
                prepend_path PATH /cluster/apps/gcc/4.8.2/bin
                prepend_path LD_LIBRARY_PATH /cluster/apps/gcc/4.8.2/lib64
                unset -f prepend_path
                source $PROFILE_DIR/modules.sh
        ;;
        new)
                source $PROFILE_DIR/z00_lmod.sh
                source $PROFILE_DIR/z01_StdEnv.sh
        ;;
        *)
                echo "Error, unknown default software stack. Please contact a system administrator"
        ;;
esac

