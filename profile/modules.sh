#----------------------------------------------------------------------#
# system-wide profile.modules                                          #
# Initialize modules for all sh-derivative shells                      #
#----------------------------------------------------------------------#
trap "" 1 2 3

case "$0" in
    -bash|bash|*/bash) . /cluster/apps/modules/init/bash ;;
       -ksh|ksh|*/ksh) . /cluster/apps/modules/init/ksh ;;
          -sh|sh|*/sh) . /cluster/apps/modules/init/sh ;;
                    *) . /cluster/apps/modules/init/sh ;;       # default for scripts
esac

module load modules

trap 1 2 3