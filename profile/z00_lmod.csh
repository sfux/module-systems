#!/bin/csh
# -*- shell-script -*-
########################################################################
#  This is the system wide source file for setting up
#  modules:
#
########################################################################

set MY_NAME="/cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/init/cshrc"



if ( ! $?MODULEPATH_ROOT ) then
    if ( $?USER) then
        setenv USER $LOGNAME
    endif

    set UNAME = `uname`
    setenv LMOD_sys             $UNAME
    setenv MODULEPATH_ROOT      "/cluster/apps/lmodules"
    setenv MODULEPATH `/cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/libexec/addto --append MODULEPATH /cluster/apps/lmodules/Core`
    setenv BASH_ENV             "/cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/init/bash"

    #
    # If MANPATH is empty, Lmod is adding a trailing ":" so that
    # the system MANPATH will be found
    if ( ! $?MANPATH ) then
      setenv MANPATH :
    endif
    setenv MANPATH `/cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/libexec/addto MANPATH /cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/share/man`

endif

if ( -f  /cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/init/csh  ) then
  source /cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/init/csh
endif

# Local Variables:
# mode: shell-script
# indent-tabs-mode: nil
# End: