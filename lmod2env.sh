#!/bin/bash

########################################################################################
#                                                                                      #
# Script to switch local enviornment from LMOD modules to environment modules on Euler #
# This implies also a switch of the standard compiler from GCC 4.8.5 to GCC 4.8.2      #
# Author: Dr. Sc. ETH Samuel Fux                                                       #
# Date  : August,2018@ETH Zurich                                                       #
#                                                                                      #
########################################################################################

# Check what module system is used in the current shell
case $MODULESHOME in
       /cluster/apps/modules)
       MS_CURRENT=env
       ;;
       /cluster/spack/apps/linux-centos7-x86_64/gcc-4.8.5/lmod*)
       MS_CURRENT=lmod
       ;;
       /cluster/apps/gcc-4.8.5/lmod*)
       MS_CURRENT=lmod
       ;;
       *)
       echo -e "Current modulesystem could not be determined"
       echo -e "\$MODULESHOME=$MODULESHOME"
       echo -e "Please logout and login again"
       return
       ;;
esac

# The code for the functions prepend_path, append_path and remove path is taken from the following stackoverflow discussion
# https://stackoverflow.com/questions/24515385/is-there-a-general-way-to-add-prepend-remove-paths-from-general-environment-vari

# generic function to prepend a path to an environment variable
prepend_path() {
    local varName=$1 fieldVal=$2 IFS=${3:-':'} auxArr
    read -ra auxArr <<< "${!varName}"
    for i in "${!auxArr[@]}"; do
        [[ ${auxArr[i]} == "$fieldVal" ]] && unset auxArr[i]
    done
    auxArr=("$fieldVal" "${auxArr[@]}")
    printf -v "$varName" '%s' "${auxArr[*]}"
}

# generic function to append a path to an environment variable
append_path() {
    local varName=$1 fieldVal=$2 IFS=${3:-':'} auxArr
    read -ra auxArr <<< "${!varName}"
    for i in "${!auxArr[@]}"; do
        [[ ${auxArr[i]} == "$fieldVal" ]] && unset auxArr[i]
    done
    auxArr+=("$fieldVal")
    printf -v "$varName" '%s' "${auxArr[*]}"
}

# generic function to remove a path from an environment variable
remove_path() {
    local varName=$1 fieldVal=$2 IFS=${3:-':'} auxArr
    read -ra auxArr <<< "${!varName}"
    for i in "${!auxArr[@]}"; do
        [[ ${auxArr[i]} == "$fieldVal" ]] && unset auxArr[i]
    done
    printf -v "$varName" '%s' "${auxArr[*]}"
}

if [ "$MS_CURRENT" == "env"  ]
then
        echo -e "Current modulesystem is already environment modules, nothing to change for lmod2env"
        return
fi

# step 1:
# clean up all traces of LMOD modules
module purge
clearMT
unset LMOD_BASEPATH
unset LMOD_sys
unset MODULEPATH_ROOT
unset BASH_ENV
remove_path MANPATH /cluster/apps/gcc-4.8.5/lmod-7.7.13-epk3osxslctnrx6gabjmwtudqm2vfbxf/lmod/lmod/share/man
unset LMOD_PKG
unset LMOD_DIR
unset LMOD_CMD
unset MODULESHOME
unset LMOD_SETTARG_FULL_SUPPORT
unset -f module
unset LMOD_VERSION
unset -f ml
unset -f clearMT
unset -f xSetTitleLmod
unset SHOST
unset __Init_Default_Modules
unset LMOD_SYSTEM_DEFAULT_MODULES
unset __LMOD_REF_COUNT_PATH

# step 2:
# again set gcc/4.8.2 as standard compiler
prepend_path INCLUDE /cluster/apps/gcc/4.8.2/include
prepend_path PATH /cluster/apps/gcc/4.8.2/bin
prepend_path LD_LIBRARY_PATH /cluster/apps/gcc/4.8.2/lib64

# step 3:
# unset functions to change environment variables
unset -f prepend_path
unset -f append_path
unset -f remove_path

# step 4:
# initialize environment modules
module() { eval `/cluster/apps/modules/bin/modulecmd bash $*`; }
export -f module
export MODULESHOME=/cluster/apps/modules
export LOADEDMODULES="";
export MODULEPATH=/cluster/apps/modules/modulefiles
. /cluster/apps/modules/init/bash_completion
module load modules
