# Module systems

Set of scripts to set a default (either environment modules [old] or LMOD [new]) module system

The script

```
software_stack_default.sh
```

needs to be stored in /etc/profile.d. It contains the global default for setting the software stack, it sets the aliases for the env2lmod.sh and lmod2env.sh scripts and it loads the corresponding scripts to initialize the software stack of choice. Please note that the paths to the location of env2lmod.sh and lmod2env.sh need to be adapted.

This script is supposed to replace the following scripts that are currently stored in /etc/profile.d

```
modulesystems.sh
modules.sh
modules.csh
default_compiler.sh
```

The scripts

```
env2lmod.sh
lmod2env.sh
set_software_stack.sh
```

need to be stored in a location that is in the $PATH of all users (currently: /cluster/apps/local). The env2lmod.sh and lmod2env.sh scripts are used to switch between the two software stacks (old/new). The set_software_stack.sh script is used to display the current default for a user and to set a permanent default based on the users choice.

The profile scripts

```
modules.sh
modules.csh
z00_lmod.sh
z00_lmod.csh
z01_StdEnv.sh
z01_StdEnv.sh
```

need to be stored in a location that is specified in software_stack_default.sh as $PROFILE_DIR (currently: /cluster/apps/.moduleprofiles)

The user can switch module systems in the current shell by using the commands

```
env2lmod
lmod2env
```

The permanent default setting for the software stack can be set with the commands

```
set_software_stack.sh old
set_software_stack.sh new
```

The current default can be displayed with the command

```
set_software_stack.sh -i
```
